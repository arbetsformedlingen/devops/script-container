FROM docker.io/library/debian:stable-20240211-slim

RUN apt-get update \
    && apt-get install -y \
        coreutils \
        wget \
        zip \
	unzip \
	curl \
	git \
	uuid \
	tar \
        telnet \
        tree \
        jq \
    && apt-get -y clean

RUN curl -o /bin/mc https://dl.min.io/client/mc/release/linux-amd64/archive/mc.RELEASE.2024-03-03T00-13-08Z \
    && chmod +x /bin/mc

ENV HOME=/tmp

WORKDIR /tmp
 
