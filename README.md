# script-container

Builds a container image with utils often used in scripts within Jobtech. 
Such as curl, git, and minio client.

Workingdir and homedir is `/tmp/`.

Command to get a pod up and running and get the prompt:

```shell
kubectl run -it debug --image=docker-images.jobtechdev.se/script-container/script-container:latest --image-pull-policy=Always
```

